import struct, socket
import hashlib
import threading, random
import time
import struct
from base64 import b64encode, b64decode

connectionlist = {}
g_code_length = 0
g_header_length = 0

def get_datalength(msg):
    global g_code_length
    global g_header_length

    g_code_length = msg[1] & 127

    if g_code_length == 126:
        g_code_length = struct.unpack('>H', msg[2:4])[0]
        g_header_length = 8
    elif g_code_length == 127:
        g_code_length = struct.unpack('>Q', msg[2:10])[0]
        g_header_length = 14
    else:
        g_header_length = 6
    g_code_length = int(g_code_length)
    return g_code_length

def parse_data(msg):
    print(msg)
    print('parse start')
    global  g_code_length
    if g_code_length == 126:
        g_code_length = struct.unpack('>H', msg[2:4])[0]
        masks = msg[4:8]
        data = msg[8:]
    elif g_code_length == 127:
        g_code_length = struct.unpack('>Q', msg[2:10])[0]
        masks = msg[10:14]
        data = msg[14:]
    else:
        masks = msg[2:6]
        data = msg[6:]

    print(masks)
    print(masks[0])
    i = 0
    raw_str = ''
    for d in data:
        raw_str += str(bytes(hex(d ^ masks[i % 4]).encode()).decode())
        i += 1
    print(raw_str)
    #for d in data:
        #print("d", d)
        #print("masks", masks[i % 4])
        #print(hex(d ^ masks[i % 4]))
        #print(hex(d ^ masks[i % 4]).encode())
        #print(bytes(hex(d ^ masks[i % 4]).encode()).decode())
        #raw_str += hex(d ^ masks[i % 4]).encode()
        #raw_str += hex(d ^ masks[i % 4]).encode()
        #i += 1
    print(u"总长度是: %d" % int(g_code_length))
    return raw_str

class WebSocket(threading.Thread):
    GUID = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"

    def __init__(self, conn, index, name, remote, path="/"):

        threading.Thread.__init__(self)

        self.conn = conn  # connection
        self.index = index  # i index
        self.name = name  # username/address[0]
        self.remote = remote  # address
        self.path = path
        self.buffer = ""
        self.buffer_utf8 = ""
        self.length_buffer = 0

    def run(self):

        print('Socket %s start' % self.index)
        headers = {}
        self.handshaken = False

        while True:

            if self.handshaken == False:

                #self.buffer += bytes.decode(self.conn.recv(1024))
                self.buffer += self.conn.recv(1024).decode()

                if self.buffer.find('\r\n\r\n') != -1:
                    header, data = self.buffer.split('\r\n\r\n', 1)
                    for line in header.split("\r\n")[1:]:
                        key, value = line.split(": ", 1)
                        headers[key] = value

                    headers["Location"] = "ws://%s%s" % (headers["Host"], self.path)

                    key = headers['Sec-WebSocket-Key']
                    # 加上固定字符串 做一次sha1再做一次bs64
                    token = b64encode(hashlib.sha1(str.encode(str(key + self.GUID))).digest())

                    handshake = "HTTP/1.1 101 Switching Protocols\r\n" \
                                "Upgrade: websocket\r\n" \
                                "Connection: Upgrade\r\n" \
                                "Sec-WebSocket-Accept: " + bytes.decode(token) + "\r\n" \
                                                                                 "WebSocket-Origin: " + str(
                        headers["Origin"]) + "\r\n" \
                                             "WebSocket-Location: " + str(headers["Location"]) + "\r\n\r\n"

                    self.conn.send(str.encode(handshake))
                    self.handshaken = True

            else:
                global g_code_length
                global g_header_length
                mm = self.conn.recv(128)
                if len(mm) <= 0:
                    continue
                if g_code_length == 0:
                    get_datalength(mm)
                self.length_buffer = self.length_buffer + len(mm)
                self.buffer = mm

                if self.length_buffer - g_header_length < g_code_length:
                    continue
                else:
                    self.buffer_utf8 = parse_data(self.buffer)
                    print(self.buffer_utf8.decode())
                    print(self.buffer_utf8)


class WebSocketServer(object):
    def __init__(self):
        self.socket = None

    def begin(self):
        print("WebSocket Start")

        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind(("127.0.0.1", 12345))
        self.socket.listen(50)

        global connectionlist

        i = 0

        while True:
            connection, address = self.socket.accept()

            username = address[0]
            newSocket = WebSocket(connection, i, username, address)
            newSocket.start()

            connectionlist['connection' + str(i)] = connection
            i = i + 1


if __name__ == "__main__":
    server = WebSocketServer()
    server.begin()
