import socket
import hashlib  
import base64  
import struct


 



sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)  
sock.bind(('127.0.0.1',8888))  
sock.listen(5) 
isHandleShake = False 
while True:
    conn, addr = sock.accept()
    while True:
    		if not isHandleShake:
    			clientData  = conn.recv(1024)
    			 
    			dataList = clientData.decode().split("\r\n")
    			header = {}
    			print(clientData.decode())
    			for data in dataList:
    				if ": " in data:
    					unit = data.split(": ")
    					header[unit[0]] = unit[1]
    			secKey = header['Sec-WebSocket-Key']
    			resKey = base64.encodestring(hashlib.new("sha1",(secKey+"258EAFA5-E914-47DA-95CA-C5AB0DC85B11").encode()).digest())
 

    			response = '''HTTP/1.1 101 Switching Protocols\r\n'''
    			response += '''Upgrade: websocket\r\n'''
    			response += '''Connection: Upgrade\r\n'''
    			response += '''Sec-WebSocket-Accept: %s\r\n'''%(resKey.decode(),)
    			##response += '''Sec-WebSocket-Protocol: chat\r\n\r\n'''
    			conn.send(response.encode())
    			
    			isHandleShake = True
    		else:
    			conn.send("welcome!".encode())
    			#data = conn.recv(1024)
    			data_head = conn.recv(1)
    			header = struct.unpack("B",data_head)[0]
    			opcode = header & 0b00001111
    			print(opcode)  
    			 
conn.close()  # 跳出循环时结束通讯 
