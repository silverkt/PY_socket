import socket

sk = socket.socket()
sk.bind(("192.168.31.235", 9008))
sk.listen(5)
while True:
    conn, addr = sk.accept()
    while True:
        accept_data = str(conn.recv(1024),
                          encoding="utf8")
        print("".join(["接收内容：", accept_data, "     客户端口：", str(addr[1])]))
        if accept_data == "byebye":  # 如果接收到“byebye”则跳出循环结束和第一个客户端的通讯，开始与下一个客户端进行通讯
            break
        send_data = input("输入发送内p容：")
        conn.sendall(bytes(send_data, encoding="utf8"))
    conn.close()  # 跳出循环时结束通讯

    ## http://www.cnblogs.com/idontknowthisperson/p/5958536.html
    ## http://www.cnblogs.com/aylin/p/5572104.html
    ## https://docs.python.org/3/library/socket.html
    ## http://www.jb51.net/article/57602.htm
    ## http://blog.csdn.net/icechenbing/article/details/7407588